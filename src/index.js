/* eslint-disable no-unused-vars */
const fs = require('fs');
const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
require('dotenv').config();

const app = express();
const mongoose = require('mongoose');

const key = process.env.KEY;
const user = process.env.USER;
mongoose.connect(`mongodb+srv://${user}:${key}@cluster0.0x11au7.mongodb.net/uber?retryWrites=true&w=majority`);

const PORT = 8080;

const { usersRouter } = require('./routes/usersRouter');
const { truckRouter } = require('./routes/truckRouter');
const { loadRouter } = require('./routes/loadRouter');

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api', usersRouter);

app.use('/api/trucks', truckRouter);

app.use('/api/loads', loadRouter);

const start = async () => {
  try {
    app.listen(PORT);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();

// ERROR HANDLER
function errorHandler(err, req, res, next) {
  res.status(500).send({ message: err.message });
}
app.use(errorHandler);
