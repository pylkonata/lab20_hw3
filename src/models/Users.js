const mongoose = require('mongoose');
const Joi = require('joi');

const userJoiSchema = Joi.object({
  role: Joi.string()
    .required(),
  email: Joi.string()
    .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } })
    .required(),
  password: Joi.string()
    .pattern(/^[a-zA-Z0-9]{3,30}$/)
    .required(),

  name: Joi.string()
    .alphanum()
    .min(2)
    .max(60),
});
const User = mongoose.model('User', {
  name: {
    type: String,
    required: false,
    lowercase: true,
  },
  role: {
    type: String,
    required: true,
    enum: ['SHIPPER', 'DRIVER'],
  },
  email: {
    type: String,
    required: [true, 'Please enter your email.'],
    unique: true,
    lowercase: true,
  },
  password: {
    type: String,
    required: true,
  },
  createdDate: {
    type: Date,
    default: Date.now,
  },
});

module.exports = {
  User,
  userJoiSchema,
};
