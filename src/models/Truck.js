const mongoose = require('mongoose');

const truckSchema = mongoose.Schema({
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  assigned_to: {
    type: mongoose.Schema.Types.ObjectId,
    default: null,
    required: false,
  },
  type: {
    type: String,
    enum: ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'],
    required: true,
  },
  status: {
    type: String,
    enum: ['OL', 'IS'],
    default: 'IS',
    required: true,
  },
  payload: {
    type: Number,
    required: false,
  },
  dimensions: {
    width: {
      type: Number,
      required: false,
    },
    length: {
      type: Number,
      required: false,
    },
    height: {
      type: Number,
      required: false,
    },
  },
}, {
  timestamps: {
    createdAt: 'created_date',
    updatedAt: false,
  },
});

const Truck = mongoose.model('truck', truckSchema);

module.exports = {
  Truck,
};
