const mongoose = require('mongoose');

const logsSchema = new mongoose.Schema({
  message: { type: String, default: 'Load created successfully' },
  date: { type: Date, default: Date.now },
});

const loadSchema = mongoose.Schema({
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  assigned_to: {
    type: mongoose.Schema.Types.ObjectId,
    default: null,
  },
  status: {
    type: String,
    enum: ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'],
    default: 'NEW',
  },
  state: {
    type: String,
    enum: ['null', 'En route to Pick Up', 'Arrived to Pick Up', 'En route to delivery', 'Arrived to delivery'],
    default: 'null',
  },
  name: {
    type: String,
    required: true,
  },
  payload: {
    type: Number,
    required: true,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  dimensions: {
    width: {
      type: Number,
      required: true,
    },
    length: {
      type: Number,
      required: true,
    },
    height: {
      type: Number,
      required: true,
    },
  },
  logs: {
    type: [logsSchema],
    default: () => ({}),
  },
}, {
  timestamps: {
    createdAt: 'created_date',
    updatedAt: false,
  },
});

const Load = mongoose.model('load', loadSchema);

module.exports = {
  Load,
};
