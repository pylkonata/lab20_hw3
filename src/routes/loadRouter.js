const express = require('express');

const router = express.Router();
const {
  addLoad, getLoadById, updateLoadById, deleteLoadById,
  postLoadById, getLoadShippingInfoById, getActiveLoad,
  changeLoadState, getLoads,
} = require('../controllers/loadService');
const { authMiddleware } = require('../middleware/authMiddleware');

const asyncWrapper = (controller) => (req, res, next) => controller(req, res, next).catch(next);

router.post('/', authMiddleware, asyncWrapper(addLoad));

router.get('/', authMiddleware, asyncWrapper(getLoads));

router.get('/active', authMiddleware, asyncWrapper(getActiveLoad));

router.patch('/active/state', authMiddleware, asyncWrapper(changeLoadState));

router.get('/:id', authMiddleware, asyncWrapper(getLoadById));

router.get('/:id/shipping_info', authMiddleware, asyncWrapper(getLoadShippingInfoById));

router.put('/:id', authMiddleware, asyncWrapper(updateLoadById));

router.post('/:id/post', authMiddleware, asyncWrapper(postLoadById));

router.delete('/:id', authMiddleware, asyncWrapper(deleteLoadById));

module.exports = {
  loadRouter: router,
};
