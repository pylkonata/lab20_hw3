const express = require('express');

const router = express.Router();
const {
  addTruckForUser, getUserTrucks, getTruckById,
  updateTruckTypeById, deleteTruckById, assignTruckToUserById,
} = require('../controllers/truckService');
const { authMiddleware } = require('../middleware/authMiddleware');

const asyncWrapper = (controller) => (req, res, next) => controller(req, res, next).catch(next);

router.post('/', authMiddleware, asyncWrapper(addTruckForUser));

router.get('/', authMiddleware, asyncWrapper(getUserTrucks));

router.get('/:id', authMiddleware, asyncWrapper(getTruckById));

router.put('/:id', authMiddleware, asyncWrapper(updateTruckTypeById));

router.post('/:id/assign', authMiddleware, assignTruckToUserById);

router.delete('/:id', authMiddleware, deleteTruckById);

module.exports = {
  truckRouter: router,
};
