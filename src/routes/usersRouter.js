const express = require('express');

const router = express.Router();
const {
  registerUser, loginUser, forgotPassword, getUserInfo, deleteUserProfile,
  changeProfilePassword,
} = require('../controllers/usersService');
const { authMiddleware } = require('../middleware/authMiddleware');

const asyncWrapper = (controller) => (req, res, next) => controller(req, res, next).catch(next);

router.post('/auth/register', asyncWrapper(registerUser));

router.post('/auth/login', asyncWrapper(loginUser));

router.post('/auth/forgot_password', asyncWrapper(forgotPassword));

router.get('/users/me', authMiddleware, getUserInfo);

router.delete('/users/me', authMiddleware, deleteUserProfile);

router.patch('/users/me/password', authMiddleware, changeProfilePassword);

module.exports = {
  usersRouter: router,
};
