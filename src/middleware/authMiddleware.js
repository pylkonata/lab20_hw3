/* eslint-disable consistent-return */
const jwt = require('jsonwebtoken');
require('dotenv').config();

const sekretKey = process.env.SECRET_KEY;

const authMiddleware = (req, res, next) => {
  const { authorization } = req.headers;

  if (!authorization) {
    return res.status(401).json({ message: 'Please, provide authorization header' });
  }

  const [, token] = authorization.split(' ');

  if (!token) {
    return res.status(401).json({ message: 'Please, include token to request' });
  }

  try {
    const tokenPayload = jwt.verify(token, sekretKey);
    req.user = {
      userId: tokenPayload.userId,
      name: tokenPayload.name,
    };
    next();
  } catch (err) {
    return res.status(401).json({ message: err.message });
  }
};

module.exports = {
  authMiddleware,
};
