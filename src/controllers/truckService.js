/* eslint-disable no-underscore-dangle */
/* eslint-disable consistent-return */
/* eslint-disable no-unused-vars */
const { Truck } = require('../models/Truck');
const { User } = require('../models/Users');

const truckSize = {
  SPRINTER: {
    length: 300,
    width: 250,
    height: 170,
  },
  'SMALL STRAIGHT': {
    length: 500,
    width: 250,
    height: 170,
  },
  'LARGE STRAIGHT': {
    length: 700,
    width: 350,
    height: 200,
  },
};
const truckPayload = {
  SPRINTER: 1700,
  'SMALL STRAIGHT': 2500,
  'LARGE STRAIGHT': 4000,
};
async function addTruckForUser(req, res, next) {
  const { userId } = req.user;
  const user = await User.findOne({ _id: userId });
  // console.log(user);
  if (user.role !== 'DRIVER') {
    return res.status(400).json({ message: 'You are\'t a driver!' });
  }
  const { type } = req.body;
  if (!type) {
    return res.status(400).json({ message: 'Please enter type' });
  }
  const truck = new Truck({
    created_by: userId,
    type,
    payload: truckPayload[type],
    dimensions: truckSize[type],
  });
  console.log(truck);
  truck.save()
    .then((saved) => res.status(200).send(
      { message: 'Truck created successfully' },
    ))
    .catch((err) => {
      res.status(400).send({ message: err.message });
    });
}

const getUserTrucks = async (req, res, next) => {
  const { userId } = req.user;
  const user = await User.findOne({ _id: userId });
  // console.log(user);
  if (user.role !== 'DRIVER') {
    return res.status(400).json({ message: 'You are\'t a driver!' });
  }
  const trucks = await Truck.find({ created_by: userId });
  console.log(trucks);
  return res.status(200).json({
    trucks,
  });
};

const getTruckById = async (req, res, next) => {
  try {
    const { userId } = req.user;
    const user = await User.findOne({ _id: userId });
    // console.log(user);
    if (!user) {
      return res.status(400).json({ message: 'Such user was not found' });
    }
    if (user.role !== 'DRIVER') {
      return res.status(400).json({ message: 'You are\'t a driver!' });
    }
    const truck = await Truck.findById(req.params.id);
    if (!truck || userId !== truck.created_by.toString()) {
      return res.status(400).json({ message: 'Truck not found' });
    }
    return res.status(200).json({ truck });
  } catch (err) {
    throw err.message;
  }
};

const updateTruckTypeById = async (req, res, next) => {
  const { userId } = req.user;
  const user = await User.findOne({ _id: userId });
  // console.log(user);
  if (!user) {
    return res.status(400).json({ message: 'Such user was not found' });
  }
  if (user.role !== 'DRIVER') {
    return res.status(400).json({ message: 'You are\'t a driver!' });
  }
  const truckOL = await Truck.findOne({ assigned_to: userId, status: 'OL' });
  if (truckOL) {
    return res.status(400).json(
      { message: 'You could not change truck info till you have on load status' },
    );
  }
  const truck = await Truck.findById(req.params.id);
  if (!truck || userId !== truck.created_by.toString()) {
    return res.status(400).json({ message: 'Truck not found' });
  }
  const { type } = req.body;
  if (!type) {
    return res.status(200).json({ message: 'Please enter new type for truck' });
  }
  truck.type = type;
  truck.payload = truckPayload[type];
  truck.dimensions = truckSize[type];
  return truck.save().then(() => res.status(200).json(
    { message: 'Truck details changed successfully' },
  ));
};

const assignTruckToUserById = async (req, res, next) => {
  const { userId } = req.user;
  const user = await User.findOne({ _id: userId });
  // console.log(user);
  if (!user) {
    return res.status(400).json({ message: 'Such user was not found' });
  }
  if (user.role !== 'DRIVER') {
    return res.status(400).json({ message: 'You are\'t a driver!' });
  }
  const truckOL = await Truck.findOne({ assigned_to: userId, status: 'OL' });
  if (truckOL) {
    return res.status(400).json(
      { message: 'You could not assign truck till you have on load status' },
    );
  }
  const truck = await Truck.findById(req.params.id);
  if (!truck || userId !== truck.created_by.toString()) {
    return res.status(400).json({ message: 'Truck not found' });
  }
  const trucks = await Truck.find({ assigned_to: userId });
  // console.log(trucks);
  if (trucks.length !== 0) {
    return res.status(400).json({ message: 'Assigned Truck already exists' });
  }
  truck.assigned_to = userId;
  return truck.save().then(() => res.status(200).json(
    { message: 'Truck assigned successfully' },
  ));
};

const deleteTruckById = async (req, res, next) => {
  const { userId } = req.user;
  const user = await User.findOne({ _id: userId });
  // console.log(user);
  if (!user) {
    return res.status(400).json({ message: 'Such user was not found' });
  }
  if (user.role !== 'DRIVER') {
    return res.status(400).json({ message: 'You are\'t a driver!' });
  }
  const truckOL = await Truck.findOne({ assigned_to: userId, status: 'OL' });
  if (truckOL) {
    return res.status(400).json(
      { message: 'You could not delete truck till you have on load status' },
    );
  }
  const truck = await Truck.findById(req.params.id);
  if (!truck || userId !== truck.created_by.toString()) {
    return res.status(400).json({ message: 'Truck not found' });
  }
  return Truck.findByIdAndDelete(req.params.id)
    .then(() => res.status(200).json(
      { message: 'Truck deleted successfully' },
    ));
};

module.exports = {
  addTruckForUser,
  getUserTrucks,
  getTruckById,
  updateTruckTypeById,
  deleteTruckById,
  assignTruckToUserById,
};
