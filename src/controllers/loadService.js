/* eslint-disable no-underscore-dangle */
/* eslint-disable quote-props */
/* eslint-disable no-unused-vars */
/* eslint-disable camelcase */
const mongoose = require('mongoose');
const { Load } = require('../models/Load');
const { Truck } = require('../models/Truck');
const { User } = require('../models/Users');

const addLoad = async (req, res) => {
  const { userId } = req.user;
  const user = await User.findOne({ _id: userId });
  // console.log(user);
  if (!user) {
    return res.status(400).json({ message: 'Such user was not found' });
  }
  if (user.role !== 'SHIPPER') {
    return res.status(400).json({ message: 'You are\'t a shipper!' });
  }
  const {
    name, payload, pickup_address, delivery_address, dimensions,
  } = req.body;
  if (!name || !payload || !pickup_address || !delivery_address || !dimensions) {
    return res.status(400).json({ message: 'Please enter load info!' });
  }
  const load = new Load({
    created_by: userId,
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions,
  });

  return load.save().then((saved) => {
    console.log(saved);
    res.status(200).json({ message: 'Load created successfully' });
  });
};

const getLoads = async (req, res, next) => {
  try {
    const { userId } = req.user;
    const { status } = req.query;
    let { limit, offset } = req.query;
    if (!limit) {
      limit = 10;
    }
    if (!offset) {
      offset = 0;
    }
    const user = await User.findOne({ _id: userId });
    console.log(user);
    if (!user) {
      return res.status(400).json({ message: 'Such user was not found' });
    }
    let loads;
    if (user.role === 'SHIPPER') {
      if (!status) {
        loads = await Load.find({ created_by: userId });
      } else {
        loads = await Load.find({ created_by: userId, status });
      }
    }
    if (user.role === 'DRIVER') {
      if (!status) {
        loads = await Load.find({ assigned_to: userId });
      } else {
        loads = await Load.find({ assigned_to: userId, status });
      }
    }
    const end = await ((loads.length - offset - limit) <= 0 ? loads.length
      : +offset + +limit);
    // console.log(end);
    // console.log(loads);
    const selected = await loads.slice(offset, end);
    // console.log(selected);
    return res.status(200).json({ loads: selected });
  } catch (err) {
    return res.status(400).json({ message: err.message });
  }
};

const getLoadById = async (req, res, next) => {
  try {
    const { userId } = req.user;
    const user = await User.findOne({ _id: userId });
    // console.log(user);
    if (!user) {
      return res.status(400).json({ message: 'Such user was not found' });
    }
    if (user.role !== 'SHIPPER') {
      return res.status(400).json({ message: 'You are\'t a shipper!' });
    }
    const load = await Load.findById(req.params.id);
    if (!load || userId !== load.created_by.toString()) {
      return res.status(400).json({ message: 'Load not found' });
    }
    console.log(load);
    return res.status(200).json({ load });
  } catch (err) {
    throw err.message;
  }
};

const updateLoadById = async (req, res, next) => {
  const { userId } = req.user;
  const user = await User.findOne({ _id: userId });
  if (!user) {
    return res.status(400).json({ message: 'Such user was not found' });
  }
  if (user.role !== 'SHIPPER') {
    return res.status(400).json({ message: 'You are\'t a shipper!' });
  }
  const load = await Load.findById(req.params.id);
  if (!load || userId !== load.created_by.toString()) {
    return res.status(400).json({ message: 'Load not found' });
  }
  if (load.status !== 'NEW') {
    return res.status(400).json(
      { message: 'You could update load only in status New' },
    );
  }
  // console.log(load);
  const {
    name, payload, pickup_address, delivery_address, dimensions,
  } = req.body;
  return Load.findByIdAndUpdate(
    req.params.id,
    {
      $set: {
        name, payload, pickup_address, delivery_address, dimensions,
      },
    },
  ).then(() => {
    res.json({ message: 'Load details changed successfully' });
  }).catch((err) => {
    next(err);
  });
};

const postLoadById = async (req, res) => {
  const { userId } = req.user;
  const user = await User.findOne({ _id: userId });
  if (!user) {
    return res.status(400).json({ message: 'Such user was not found' });
  }
  if (user.role !== 'SHIPPER') {
    return res.status(400).json({ message: 'You are\'t a shipper!' });
  }
  const load = await Load.findById(req.params.id);
  if (!load || userId !== load.created_by.toString()) {
    return res.status(400).json({ message: 'Load not found' });
  }
  if (load.status !== 'NEW') {
    return res.status(400).json(
      { message: 'You could post load only in status New' },
    );
  }
  const loadUpdated = await Load.findByIdAndUpdate(req.params.id, { $set: { status: 'POSTED' } });
  const trucks = await Truck.aggregate([
    {
      '$match': {
        'assigned_to': {
          '$ne': null,
        },
        'status': 'IS',
        'payload': { $gt: load.payload },
        'dimensions': { $gt: load.dimensions },
        'dimensions.length': {
          '$gt': load.dimensions.length,
        },
        'dimensions.width': {
          '$gt': load.dimensions.width,
        },
        'dimensions.height': {
          '$gt': load.dimensions.height,
        },
      },
    }, {
      '$sort': {
        'payload': 1,
      },
    },
  ]);
  console.log(trucks);
  if (trucks.length === 0) {
    loadUpdated.status = 'NEW';
    loadUpdated.logs.push({ message: 'Truck not found' });
    await loadUpdated.save();
    return res.status(200).json(
      {
        message: 'Load posted successfully',
        driver_found: false,
      },
    );
  }
  const truck = await Truck.findById(trucks[0]._id);
  // console.log(truck);
  truck.status = 'OL';
  await truck.save();
  loadUpdated.assigned_to = truck.assigned_to;
  loadUpdated.status = 'ASSIGNED';
  loadUpdated.state = 'En route to Pick Up';
  loadUpdated.logs.push(
    {
      message: `Load assigned to driver with id ${trucks[0].assigned_to.toString()}`,
    },
  );
  await loadUpdated.save();
  // console.log(trucks);
  return res.status(200).json(
    {
      message: 'Load posted successfully',
      driver_found: true,
    },
  );
};

const deleteLoadById = async (req, res, next) => {
  const { userId } = req.user;
  const user = await User.findOne({ _id: userId });
  if (!user) {
    return res.status(400).json({ message: 'Such user was not found' });
  }
  if (user.role !== 'SHIPPER') {
    return res.status(400).json({ message: 'You are\'t a shipper!' });
  }
  const load = await Load.findById(req.params.id);
  if (!load || userId !== load.created_by.toString()) {
    return res.status(400).json({ message: 'Load not found' });
  }
  if (load.status !== 'NEW') {
    return res.status(400).json(
      { message: 'You could delete load only in status New' },
    );
  }
  return Load.findByIdAndDelete(req.params.id)
    .then(() => res.status(200).json({ message: 'Load deleted successfully' }));
};

const getLoadShippingInfoById = async (req, res, next) => {
  const { userId } = req.user;
  const user = await User.findOne({ _id: userId });
  if (!user) {
    return res.status(400).json({ message: 'Such user was not found' });
  }
  if (user.role !== 'SHIPPER') {
    return res.status(400).json({ message: 'You are\'t a shipper!' });
  }
  const load = await Load.findById(req.params.id);
  if (!load || userId !== load.created_by.toString()) {
    return res.status(400).json({ message: 'Load not found' });
  }
  // console.log(load.assigned_to);
  const truck = await Truck.findOne({ assigned_to: load.assigned_to });
  console.log(truck);
  return res.status(200).json({ load, truck });
};

const getActiveLoad = async (req, res, next) => {
  const { userId } = req.user;
  const user = await User.findOne({ _id: userId });
  if (!user) {
    return res.status(400).json({ message: 'Such user was not found' });
  }
  // console.log(user);
  if (user.role !== 'DRIVER') {
    return res.status(400).json({ message: 'You are\'t a driver!' });
  }
  const activeLoad = await Load.aggregate([
    {
      '$match': {
        'assigned_to': mongoose.Types.ObjectId(userId),
        $or: [{ status: 'ASSIGNED' }, { status: 'POSTED' }],
      },
    },
  ]);
  // console.log(activeLoad);
  if (activeLoad.length === 0) {
    return res.status(400).json({ message: 'Here is no active load for you' });
  }
  return res.status(200).json({ load: activeLoad[0] });
};
const changeLoadState = async (req, res, next) => {
  const { userId } = req.user;
  const user = await User.findOne({ _id: userId });
  if (!user) {
    return res.status(400).json({ message: 'Such user was not found' });
  }
  // console.log(user);
  if (user.role !== 'DRIVER') {
    return res.status(400).json({ message: 'You are\'t a driver!' });
  }
  const load = await Load.findOne({ assigned_to: user._id });
  console.log(load);
  if (!load) {
    return res.status(400).json({ message: 'Load was not found' });
  }
  const state = ['En route to Pick Up', 'Arrived to Pick Up', 'En route to delivery', 'Arrived to delivery'];
  const index = state.indexOf(load.state);
  console.log(index);
  if (index === 3) {
    return res.status(400).json({ message: 'this load is shipped already' });
  }
  load.state = state[index + 1];
  load.logs.push(
    {
      message: `Load state was change to ${state[index + 1]}`,
    },
  );
  if (index === 2) {
    load.status = 'SHIPPED';
    const truck = await Truck.findOne({ assigned_to: user._id });
    truck.status = 'IS';
    truck.save();
  }
  load.save();
  return res.status(200).json(
    { message: "Load state changed to 'En route to Delivery'" },
  );
};

module.exports = {
  addLoad,
  getLoads,
  getLoadById,
  updateLoadById,
  deleteLoadById,
  postLoadById,
  getLoadShippingInfoById,
  getActiveLoad,
  changeLoadState,
};
