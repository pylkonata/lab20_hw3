/* eslint-disable no-underscore-dangle */
/* eslint-disable no-unused-vars */
const jwt = require('jsonwebtoken');
const bcryptjs = require('bcryptjs');
require('dotenv').config();
const sgMail = require('@sendgrid/mail');
const generator = require('generate-password');

sgMail.setApiKey(process.env.SENDGRID_API_KEY);
const { User, userJoiSchema } = require('../models/Users');
const { Truck } = require('../models/Truck');

const sekretKey = process.env.SECRET_KEY;

const registerUser = async (req, res, next) => {
  const { email, password, role } = req.body;
  await userJoiSchema.validateAsync({ email, password, role });
  const name = email.split('@')[0];
  console.log(name);
  const user = new User({
    email,
    name,
    password: await bcryptjs.hash(password, 10),
    role,
  });
  user.save()
    .then((saved) => res.status(200).send(
      { message: 'Profile created successfully' },
    ))
    .catch((err) => {
      res.status(400).send({ message: err.message });
    });
};

const loginUser = async (req, res, next) => {
  const { email, password } = req.body;
  console.log(email);
  const user = await User.findOne({ email });
  console.log(user);
  if (!user) {
    return res.status(400).json({ message: 'User with such email wasn\'t found' });
  }
  if (user && await bcryptjs.compare(String(password), String(user.password))) {
    const payload = {
      name: user.name,
      userId: user._id,
    };
    const jwtToken = jwt.sign(payload, sekretKey);
    return res.json({
      jwt_token: jwtToken,
    });
  }
  return res.status(403).json({ message: 'Not authorized' });
};

const forgotPassword = async (req, res, next) => {
  const { email } = req.body;
  const user = await User.findOne({ email });
  if (!user) {
    return res.status(400).json({ message: 'User wasn\'t found' });
  }
  const passwordNew = generator.generate({
    length: 10,
    numbers: true,
  });
  const msg = {
    to: 'nodejs.homework3@gmail.com',
    from: 'nodejs.homework3@gmail.com',
    subject: 'Reset password',
    text: `Your new password - ${passwordNew}`,
  };
  await sgMail.send(msg);
  const hashedPassword = await bcryptjs.hash(passwordNew, 10);
  return User.findOneAndUpdate(
    { email },
    { $set: { password: hashedPassword } },
  ).then(() => {
    res.status(200).json({ message: 'New password sent to your email address' });
  });
};

const getUserInfo = async (req, res, next) => {
  const user = await User.findOne({ _id: req.user.userId });
  console.log(user);
  if (!user) {
    return res.status(400).json({ message: 'User wasn\'t found' });
  }
  const {
    _id, role, email, createdDate,
  } = user;
  return res.status(200).json({
    user: {
      _id,
      role,
      email,
      created_date: createdDate,
    },
  });
};

const deleteUserProfile = async (req, res, next) => {
  const { userId } = req.user;
  const user = await User.findOneAndDelete({ _id: userId });
  const truck = await Truck.findOne({ assigned_to: userId, status: 'OL' });
  if (truck) {
    return res.status(400).json(
      { message: 'You could not delete your profile till you have on load status' },
    );
  }
  if (!user) {
    return res.status(400).json({ message: 'User wasn\'t found' });
  }
  return res.status(200).json({ message: 'Profile deleted successfully' });
};

const changeProfilePassword = async (req, res, next) => {
  const { oldPassword, newPassword } = req.body;
  if (!oldPassword || !newPassword) {
    return res.status(400).json({ message: 'Please enter old and new passwords' });
  }
  if (!await User.findOne({ _id: req.user.userId })) {
    return res.status(400).json({ message: 'User wasn\'t found' });
  }
  const hashedPassword = await bcryptjs.hash(newPassword, 10);
  return User.findOneAndUpdate(
    { _id: req.user.userId },
    { $set: { password: hashedPassword } },
  ).then(() => {
    res.status(200).json({ message: 'Password changed successfully' });
  });
};

module.exports = {
  registerUser,
  loginUser,
  forgotPassword,
  getUserInfo,
  deleteUserProfile,
  changeProfilePassword,
};
