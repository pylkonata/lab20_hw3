# Task 3 Node.js

This study project implements UBER like service for freight trucks, in REST style, using MongoDB as database.
This service should help regular people to deliver their stuff and help drivers to find loads and earn some money. Application contains 2 roles, driver and shipper.
REST API rules described file - openapi.yaml. You could open it in Swagger/Postman.

## The used Technologies:
- node.js
- express.js
- MongoDB, mongoose (Mongo Atlas)
- JWT
- Postman
- dotenv
- bcryptjs
 
## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.
Open [http://localhost:8080](http://localhost:8080) to view it in your browser.

### `npm run dev`
Runs the app in development mode automatically restarting the application when file changes in the directory are detected.
